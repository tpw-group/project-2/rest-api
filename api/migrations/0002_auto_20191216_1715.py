# Generated by Django 3.0 on 2019-12-16 17:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
    ]
